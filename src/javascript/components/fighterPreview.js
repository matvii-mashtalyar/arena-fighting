import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage = createFighterImage(fighter);
  const fighterName = createFighterName(fighter);
  const fighterHealth = createFighterHealth(fighter);
  const fighterAttack = createFighterAttack(fighter);
  const fighterDefense = createFighterDefense(fighter);
  fighterElement.append(fighterImage, fighterName, fighterHealth, fighterAttack, fighterDefense);

  return fighterElement;
}

function createFighterName(fighter) {
  const fighterName = createElement({ tagName: 'span', className: 'fighter-preview___name' });
  fighterName.innerText = fighter.name;
  return fighterName;
}

function createFighterHealth(fighter) {
  const fighterHealth = createElement({ tagName: 'span', className: 'fighter-preview___prop health' });
  fighterHealth.innerText = fighter.health;
  return fighterHealth;
}

function createFighterAttack(fighter) {
  const fighterAttack = createElement({ tagName: 'span', className: 'fighter-preview___prop attack' });
  fighterAttack.innerText = fighter.attack;
  return fighterAttack;
}

function createFighterDefense(fighter) {
  const fighterDefense = createElement({ tagName: 'span', className: 'fighter-preview___prop defense' });
  fighterDefense.innerText = fighter.defense;
  return fighterDefense;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
