import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const firstFighterBar = document.getElementById('left-fighter-indicator');
  firstFighterBar.style.width = '100%';
  const secondFighterBar = document.getElementById('right-fighter-indicator');
  secondFighterBar.style.width = '100%';
  let pressed = new Set();
  firstFighter.reloadCriticalHit = false;
  secondFighter.reloadCriticalHit = false;
  document.addEventListener(
    'keydown',
    (event) => {
      const keyName = event.code;

      switch (keyName) {
        case controls.PlayerOneAttack:
          if (!secondFighter.block && !firstFighter.block) {
            hit(firstFighter, secondFighter, secondFighterBar);
          }
          break;
        case controls.PlayerTwoAttack:
          if (!secondFighter.block && !firstFighter.block) {
            hit(secondFighter, firstFighter, firstFighterBar);
          }
          break;
        case controls.PlayerOneBlock:
          firstFighter.block = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.block = true;
          break;
      }
      pressed.add(keyName);

      criticalHit(firstFighter, secondFighter, secondFighterBar, pressed, controls.PlayerOneCriticalHitCombination);
      criticalHit(secondFighter, firstFighter, firstFighterBar, pressed, controls.PlayerTwoCriticalHitCombination);
    },
    false
  );

  document.addEventListener('keyup', (event) => {
    const keyName = event.code;
    pressed.delete(keyName);
    switch (keyName) {
      case controls.PlayerOneBlock:
        firstFighter.block = false;
        break;
      case controls.PlayerTwoBlock:
        secondFighter.block = false;
        break;
    }
  });

  return new Promise((resolve) => {
    document.addEventListener('keydown', () => {
      if (firstFighterBar.style.width == '0%') {
        resolve(secondFighter);
      } else if (secondFighterBar.style.width == '0%') {
        resolve(firstFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const getDamage = getHitPower(attacker) - getBlockPower(defender);
  return getDamage < 0 ? 0 : getDamage;
}

function getCritDamage(attacker) {
  const getCritDamage = getCritHit(attacker);
  return getCritDamage;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomNumber(1, 2);
  const power = fighter.attack * criticalHitChance;
  return power;
}

function getCritHit(fighter) {
  return fighter.attack * 2;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumber(1, 2);
  const power = fighter.defense * dodgeChance;
  return power;
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

function hit(attacker, defender, healthBar) {
  let health = healthBar.style.width;
  health = health.substring(0, health.length - 1);
  health = Number(health) - (getDamage(attacker, defender) / defender.health) * 100;
  if (health < 0) health = 0;
  healthBar.style.width = health + '%';
}

function criticalHit(attacker, defender, healthBar, pressed, controls) {
  for (let code of controls) {
    if (!pressed.has(code)) {
      return;
    }
  }
  pressed.clear();
  if (attacker.reloadCriticalHit) {
    return;
  }
  attacker.reloadCriticalHit = true;
  setTimeout(() => {
    attacker.reloadCriticalHit = false;
  }, 10000);

  let health = healthBar.style.width;
  health = health.substring(0, health.length - 1);
  health = Number(health) - (getCritDamage(attacker) / defender.health) * 100;
  if (health < 0) health = 0;
  healthBar.style.width = health + '%';
}
